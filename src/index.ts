#!/usr/bin/env node

import * as shellJs from "shelljs";
import * as fs from "fs-extra";
import * as tmp from "tmp";
import path from "path";

import nycRc from "./.nycrc.json";
import mochaRc from "./.mocharc.json";

const outputFolder = "reports/coverage";

const projectFolder = getProjectFolder();

const npxFolder =
    process.env.CI_PROJECT_URL !== undefined
        ? path.normalize(`${__dirname}../../../../../../`)
        : path.normalize(`${__dirname}../../../`);

function removeOldFolder(): void {
    const result = shellJs.rm("-rf", outputFolder);
    if (result.code !== 0) {
        throw new Error("Can't delete the old output");
    }
}

function createNycJson(): string {
    const dir = tmp.dirSync();
    const path = `${dir.name}/nyc.json`;

    // Override some parts of the config JSON
    nycRc["report-dir"] = `${projectFolder}/${outputFolder}`;
    nycRc["temp-dir"] = `${projectFolder}/${outputFolder}/nyc_temp_dir`;
    nycRc.extends = `${npxFolder}/node_modules/@istanbuljs/nyc-config-typescript`;

    nycRc.exclude.pop();

    // We don't want to test the test files
    nycRc.exclude.push("**/*.spec.ts");
    nycRc.exclude.push("**/*.spec.tsx");

    // We don't want to test the scripts and the configs we use
    nycRc.exclude.push("scripts/**");
    nycRc.exclude.push("config/**");

    // We don't want to test the `index.tsx` file (main)
    nycRc.exclude.push("src/index.tsx");

    // We don't want to test the generated outputs
    nycRc.exclude.push("lib/**");
    nycRc.exclude.push("build/**");
    nycRc.exclude.push("reports/");

    nycRc.cwd = `${projectFolder}`;

    fs.writeFileSync(path, JSON.stringify(nycRc));
    return path;
}

function createMochaJson(): string {
    const dir = tmp.dirSync();
    const path = `${dir.name}/mocha.json`;

    mochaRc.require.pop();
    // ESM imports
    mochaRc.require.push(`${npxFolder}/node_modules/esm`);
    // TypeScript
    mochaRc.require.push(`${npxFolder}/node_modules/ts-node/register`);
    // Source map handling
    mochaRc.require.push(`${npxFolder}/node_modules/source-map-support/register`);

    fs.writeFileSync(path, JSON.stringify(mochaRc));
    return path;
}

function getProjectFolder(): string {
    const result = shellJs.pwd();
    if (result.code !== 0) {
        throw new Error("Can't get the project folder path");
    }
    return result.stdout;
}

const nycJsonPath = createNycJson();
const mochaJsonPath = createMochaJson();

function getMochaRunCommand(): string {
    const parts: string[] = [];

    parts.push(`${npxFolder}/node_modules/.bin/mocha`);
    parts.push(`--config ${mochaJsonPath}`);
    parts.push(`"${projectFolder}/{,!(node_modules)/**/}*.spec.ts*(x)"`);
    return parts.join(" ");
}

function runNyc(): shellJs.ShellString {
    const mochaCommand = getMochaRunCommand();

    const nycPath = path.normalize(`${npxFolder}/node_modules/nyc/bin/nyc.js`);

    const result = shellJs.exec(`node ${nycPath} --nycrc-path ${nycJsonPath} ${mochaCommand}`);

    if (result.code !== 0) {
        throw new Error("Nyc error");
    }
    return result;
}

async function main(): Promise<void> {
    try {
        console.log("Generating test coverage");

        removeOldFolder();
        runNyc();

        console.log();
        console.log("Test coverage check finished.");
        console.log();
        shellJs.exit(0);
    } catch (error) {
        console.log();
        console.log(`ERROR WITH TEST COVERAGE: ${(error as Error).message}`);
        console.log();
        shellJs.exit(1);
    }
}

main();
