# @ridesz/usual-test-coverage-checker

The typical unit test and cover checking config I use for my TypeScript projects.

## How to use

### `package.json` setup

Just please add this `scripts` tag part to your `package.json` file too:

```json
{
    "test:unit": "npx @ridesz/usual-test-coverage-checker",
}
```

### GitLab CI config

Please add the necessary parts to your `.gitlab-ci.yml` file:

```yml
image: node:latest

stages:
    - test

dependecy:
    stage: test
    artifacts:
        paths:
            - build/
        expire_in: 1 week
        when: always
    script:
        - npm install
        - npm rebuild
        - npm run test:unit
```

This would use the latest node image and do the dependency graph check as part of the `test` stage.

### Running manually


If you would like to run the unit testing manually then you can use the following command:

```bash
npm run test:unit
```

## Some implementation details

The result of the check is generated into the `reports/cover` folder.

The checker only checks the sources from the `src` folder and skips the `*.spec.ts` and `*.spec.tsx` files.
